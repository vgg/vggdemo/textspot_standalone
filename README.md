# Text Spotting Software v1.4
(c) University of Oxford, 2016

This documents contains the information on the installation and operation of the 
software application. It is very important that you read and follow the instructions 
in this document before running the software application.

## System Requirements:
--------------------
### Hardware:
* At least 5G RAM
* At least 6G hard-drive space for the application itself.  i.e., the location in which the software application is stored should have at least 6G of hard-drive space available for the application itself. Any space required for the Matlab Compiler Runtime (see below) is extra.
* GPU CAN be used with this version (v1.4). The GPU should:
  - have at least 6GB of memory.
  - be an NVIDIA GPU / compatible with CUDA.

### Software:
* This software application can only be run on **Linux 64-bit operating systems**
* This software application requires MATLAB runtime libraries. These libraries can be obtained in two ways:
  1. Through full "MATLAB R2016a" installation. MATLAB is NOT a free software application.
  2. By installing "MATLAB Compiler Runtime (MCR) R2016a v9.0.1”. This IS a free software application and can be obtained at: http://www.mathworks.com/products/compiler/mcr/index.html . Please see `Installation Instructions` section for more information on installing this.
We recommend that you use option (2) if you do not already have license for the full MATLAB R2016a.
* For using GPU, CUDA 8.0 should be installed on the system. Further, CUDNN v5.1 or higher should be installed.

## Installation Instructions:
--------------------------

### Step 0:
Download the [latest source code](https://gitlab.com/vggdemo/textspot_standalone/repository/archive.zip?ref=master)

### Step 1:
If you do not already have MATLAB R2016a version, please download the **"MATLAB Compiler Runtime (MCR) R2016a version 9.0.1”** from: http://www.mathworks.com/products/compiler/mcr/index.html . Please download the Linux 64-bit version. Please follow the installation instructions at : http://uk.mathworks.com/help/compiler/install-the-matlab-runtime.html

IMPORTANT : Please remember the location where this MCR is installed. This is the ```<mcr-directory>``` location required when running the application (see `Running the Application` section).

### Step 2:
Unzip the textspot.zip file in a location with more than 5G of storage space. Delete the readpic.zip file from the system.

### Step 3:
To use this software with a GPU, make sure that CUDA 8.0 libraries can be found by MATLAB by adding them to the `LD_LIBRARY_PATH` environment variable.
> For example, if cuda-8.0 is installed in /usr/local/cuda-8.0, then:  add the line `export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/cuda-8.0` to your `$HOME/.bashrc`

Similarly, the library files of CUDNN v5.1 (or higher) should be on the `LD_LIBRARY_PATH`.

## Running the Application:
-------------------------
**We recommend that you see `Working Example Script` section below for a full example of running this software application**

To run this software application, type the following on the terminal:

```
./run.sh <mcr-directory> [FLAGS]
```
where 
 * `<mcr-directory>`   : is the directory where version 9.0.1 of MCR is installed. If you have the full version of matlab installed then this is the location of MATLAB's root folder. For example, If you have MCR installed in /mathworks/home/application/v901 then run it as:
>./run.sh /mathworks/home/application/v901 [FLAGS]

If you have MATLAB installed in /mathworks/devel/application/matlab, run as:
>./run.sh /mathworks/devel/application/matlab [FLAGS]

##### FLAGS
 * `--input (-i)` : [REQUIRED] relative/ absolute path to file containing the names of input images
 * `--output (-o)` : [REQUIRED} desired name of the output xml file. This file-name must end in '.xml'. For example, a valid value is : `output.xml`
 * `--mode (-m)` : [(optional) DEFAULT : `opt`]  one of the following:
   - `precise`: less false positives but miss harder instances
   - `most`   : detect most but allow a lot of false positives
   - `opt`    : output a balanced set
 * `--verbose (-v)` : [(optional) DEFAULT : false]    show text status messages
 * `--gpuID (-g)` : [(optional) DEFAULT : 0]        GPU device number (0 for no GPU)
 * `--slow (-s)` : [(optional) DEFAULT : false]    turn-off fast region proposal
 * `--noseq (-n)` : [(optional) DEFAULT : false]    do not perform unconstrained text recognition
 * `--help (-h)` : show this help message


## An example setup
--------------------
 * version 9.0.1 of the MCR is installed in `/mathworks/home/application/v901`
 * the input file is at `/path/to/input/frames.txt`
 * the desired output is `/path/to/annotation/output.xml`
 * the selected mode is `opt`
 * status messages are needed, then

run the shell script as : 
```
./run.sh /mathworks/home/application/v901 --input /path/to/input/frames.txt --output /path/to/annotation/output.xml --mode opt --verbose
```

## Working Example Script
-----------------------
Please examine the script `example/example_{gpu, no_gpu}.sh` for a full working examples.

This example script shows how to perform text-spotting on multiple frames:
 - It process two images : `example/frames/{1,2}.jpg`
 - The names of these images are specified in `example/frames.txt`. Edit this file to add more images / process just one image.
 - The expected output is included in `example/out-reference.xml`

To run the example WITHOUT any GPU:
 * change the `matroot` variable in example.sh to point to the Matlab Compiler Runtime installation.
 * on the terminal type:
 >./example_no_gpu.sh
 
To run the example WITH the GPU:
 * change the `matroot` variable in example.sh to point to the Matlab Compiler Runtime installation.
 * change the `--gpuID` option to the GPU device number: This is a number between 1 and N (N being the total number of GPUs in the system as listed by the command `nvidia-smi’).

on the terminal type:
>./example_gpu.sh

These should generate an output file called out.xml

## Output Format:
--------------
The output xml file contains the detected text-annotations along with their bounding-box coordinates in the following format: `height="66" width="202" x="217" y="646"`

See example/out-reference.xml for an example output generated by the application where
 * x,y are the coordinates (in pixels) of the top-left corner of the bounding box. (0,0) means the top-left corner of the image.
 * width, height : width and height of the bounding box in pixels.

